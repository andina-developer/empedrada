<?php 

function empedrada_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'empedrada' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'empedrada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'empedrada_widgets_init' );


function empedrada_widgets_footer_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer One', 'empedrada' ),
		'id'            => 'sidebar-footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'empedrada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Two', 'empedrada' ),
		'id'            => 'sidebar-footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'empedrada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Three', 'empedrada' ),
		'id'            => 'sidebar-footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'empedrada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Four', 'empedrada' ),
		'id'            => 'sidebar-footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'empedrada' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'empedrada_widgets_footer_init' );

