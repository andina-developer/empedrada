<?php 

// How to use
// pll_register_string($group, $string, $name );
//
// -php| echo pll__("Contacta con nosotros") |-php


// =======
// GLOBAL - HEADER
// =======
pll_register_string("Header", "Reservar Online", "Header Action" );

// =======
// GLOBAL - FOOTER
// =======
pll_register_string("Footer", "Síguenos en", "Social Media" );
pll_register_string("Footer", "Hecho por", "Credits" );


// =======
// GLOBAL - CTA
// =======
pll_register_string("CTA", "¿Qué esperas? has tu reservación ahora", "SubFooter" );
pll_register_string("CTA", "Reservar", "General" );

// =======
// GLOBAL - ADDREESS
// =======
pll_register_string("Address", "Hotel en Caral, Lima", "Address" );
pll_register_string("Address", "rodeado de una impresionante vista al valle de caral y a los campos de cultivo de paltos", "Address" );
// pll_register_string("Address", "Dirección", "Address" );
// pll_register_string("Address", "Teléfono", "Phone" );
// pll_register_string("Address", "Coordenadas", "Phone" );


// =======
// HOME > HERO
// =======
pll_register_string("Home", "Haz tu reserva Online", "Call To Action" );


// =======
// HOME > GALLERY
// =======
pll_register_string("Home", "Inspirate", "Galeria" );
pll_register_string("Home", "Ver más", "Galeria" );


// =======
// HOME > OFFERS
// =======
pll_register_string("Home", "Promociones", "Promociones" );
pll_register_string("Home", "BENEFICIESE DE NUESTRAS OFERTAS Y PROMOCIONES Y DESCRUBRA CARAL AL MEJOR PREIO, ¡LE ESTAMOS ESPERANDO!", "Promociones" );

// =======
// LODGE > TARIFAS
// =======

pll_register_string("Lodge", "Tarifas del Hotel", "Tarifas" );
pll_register_string("Lodge", "CONSULTA LAS TARIFAS DEL HOTEL", "Tarifas" );

// =======
// LODGE > SERVICIOS
// =======
pll_register_string("Lodge", "Servicios del Hotel", "Servicios" );
pll_register_string("Lodge", "ESTOS SON NUESTROS SERVICIOS", "Servicios" );

pll_register_string("Lodge", "Parking", "Servicios" );
pll_register_string("Lodge", "Admite pago con tarjetas", "Servicios" );
pll_register_string("Lodge", "Piscina", "Servicios" );
pll_register_string("Lodge", "Servicio de Limpieza", "Servicios" );
pll_register_string("Lodge", "Sabanas y Toallas", "Servicios" );
pll_register_string("Lodge", "Ventilador en las habitaciones", "Servicios" );
pll_register_string("Lodge", "Consigna de equipajes", "Servicios" );
pll_register_string("Lodge", "Jardín", "Servicios" );


// =======
// LODGE > LOCACIÓN
// =======
pll_register_string("Lodge", "Locación", "Locación" );
pll_register_string("Lodge", "RODEADO DE UNA IMPRESIONANTE VISTA AL VALLE DE CARAL Y A LOS CAMPOS DE CULTIVO DE PALTOS", "Locación" );
pll_register_string("Lodge", "Dirección", "Locación" );
pll_register_string("Lodge", "Teléfono", "Locación" );
pll_register_string("Lodge", "Coordenadas", "Locación" );


// =======
// CPT > ROOMS
// =======
pll_register_string("Rooms", "CONSULTA LAS TARFIAS DEL HOTEL", "Header" );
pll_register_string("Rooms", "ver disponibilidad", "Header" );
pll_register_string("Rooms", "ACOMODACIONES", "Acomodaciones" );
pll_register_string("Rooms", "DE HABITACIÓN", "Acomodaciones" );
pll_register_string("Rooms", "Otras recomendaciones", "Recomendaciones" );

// =======
// GALLERY > TABS
// =======
pll_register_string("Gallery", "Habitaciones", "Tabs" );
pll_register_string("Gallery", "Baños", "Tabs" );
pll_register_string("Gallery", "Vista", "Tabs" );
