<?php 

// EDGARDO ADD CUSTOM SHORTCODE FOR GALLERY

function empedrada_shortcode_gallery_baths() {
//Uso: [recientes  limite="3" thumbnail="1" tamano="50" tag="tagname"]
//thumbnail="1" muestra imagen destacada. thumbnail="0" no muestra la imagen
    extract(shortcode_atts(array(
        'limite' => 24,
        'thumbnail' => 1,
        'tamano' => 350,
        'post_type' => array('emp-gallery'),
        'tag' => 'tagbaths'
    ), $atts));
 
    $query = array(
                    'post_type' => $post_type,
                    'tag' => $tag,
                    'orderby' => 'id', 
                    'order' => 'ASC',
                );
 
    $q = new WP_Query($query);
    if ($q->have_posts()) :
    $salida  = '';
 

    /* comienzo while */
    $salida .= '<div class="empedrada-section container">';
    $salida .= '<div class="empedrada-section-row row">';
    
        while ($q->have_posts()) : $q->the_post();
        if ( has_post_thumbnail() && $thumbnail == true):
            // START SINGLE IMAGE
            $salida .= '<div class="gallery-page-item col-lg-4 col-md-4 col-sm-4 col-xs-6">';
               
               $salida .= get_the_post_thumbnail(get_the_id(),array($tamano,$tamano),array('title'=>get_the_title(),'alt'=>get_the_title(),'class'=>'imageborder'));

            endif;
        
            $salida .= '</div>';
            // ENDS SINGLE IMAGE
        endwhile;
    
    wp_reset_query();
    /* fin while */
 
    $salida .= '</div>';
    $salida .= '</div>';
    endif;
 
    return $salida;

}
add_shortcode('empedrada-gallery-baths', 'empedrada_shortcode_gallery_baths');

