<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Empedrada_Lodge
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function empedrada_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'empedrada_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function empedrada_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'empedrada_pingback_header' );


// Add Custom Post Types

// CUSTOM POST TYPES

// Edgardo's CUSTOM ROOM

function empedrada_rooms() {
        $labels = array(
        'name' => _x( 'Habitaciones', 'post type general name' ),
        'singular_name' => _x( 'habitacion', 'post type singular name' ),
        'add_new' => _x( 'Añadir habitacion', 'book' ),
        'add_new_item' => __( 'Añadir nuevo Habitación' ),
        'edit_item' => __( 'Editar Habitación' ),
        'new_item' => __( 'Nuevo Habitación' ),
        'view_item' => __( 'Ver Habitación' ),
        'search_items' => __( 'Buscar Habitaciones' ),
        'not_found' =>  __( 'No se han encontrado habitaciones' ),
        'not_found_in_trash' => __( 'No se han encontrado habitaciones en la papelera' ),
        'parent_item_colon' => ''
    );
 
    // Creamos un array para $args
    $args = array( 'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-analytics',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );
 
    register_post_type( 'habitacion', $args ); 
}

add_action( 'init', 'empedrada_rooms' );

// Edgardo's CUSTOM GALLERY

function empedrada_gallery() {
        $labels = array(
        'name' => _x( 'Empedrada Galeria', 'post type general name' ),
        'singular_name' => _x( 'galeria', 'post type singular name' ),
        'add_new' => _x( 'Añadir imagen de galeria', 'book' ),
        'add_new_item' => __( 'Añadir nueva imagen a galeria' ),
        'edit_item' => __( 'Editar Imagen' ),
        'new_item' => __( 'Nueva Imagen' ),
        'view_item' => __( 'Ver Imagen' ),
        'search_items' => __( 'Buscar Imagen' ),
        'not_found' =>  __( 'No se han encontrado imagenes' ),
        'not_found_in_trash' => __( 'No se han encontrado imagenes en la papelera' ),
        'parent_item_colon' => ''
    );
 
    // Creamos un array para $args
    $args = array( 'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-format-gallery',
        'taxonomies' => array( 'category_gallery', 'post_tag'),
        'supports' => array( 'title', 'author', 'thumbnail', 'excerpt' )
    );
 
    register_post_type( 'emp_gallery', $args ); 
}

add_action( 'init', 'empedrada_gallery' );


function create_eventcategory_taxonomy() {
 
$labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'popular_items' => __( 'Popular Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Category' ),
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'separate_items_with_commas' => __( 'Separate categories with commas' ),
    'add_or_remove_items' => __( 'Add or remove categories' ),
    'choose_from_most_used' => __( 'Choose from the most used categories' ),
);
 
register_taxonomy('category_gallery','cat_gallery', array(
    'label' => __('Etiquetas Galeria'),
    'labels' => $labels,
    'hierarchical' => true,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'taxonomia-galeria' ),
));
}
 
add_action( 'init', 'create_eventcategory_taxonomy', 0 );