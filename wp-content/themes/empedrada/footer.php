<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Empedrada_Lodge
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		
		<div class="footer-container container">
			
			<div class="foote-widget-container row">
				<div class="widget-item sidebar col-lg-3">
					<?php dynamic_sidebar( 'sidebar-footer-1' ); ?>
				</div>
				<div class="widget-item col-lg-3">
					<?php dynamic_sidebar( 'sidebar-footer-2' ); ?>				
				</div>
				<div class="widget-item col-lg-3">
					<?php dynamic_sidebar( 'sidebar-footer-3' ); ?>
				</div>
				<div class="widget-item col-lg-3">
					<!-- <?php dynamic_sidebar( 'sidebar-footer-4' ); ?> -->
					<div class="button-transparent"><a href="<?php empedrada_enlace_reserva(); ?>"><p><?php echo pll__("Reservar Online"); ?></p></a></div>
				</div>
			</div>

			<div class="footer-social-container">
				<div class="footer-social-main">
					
					<div class="footer-social-p">
						<p><?php echo pll__("Síguenos en"); ?></p>

					</div>
					
					<div class="footer-social-icons">
						<a href="https://www.facebook.com/hotelcaral/" target="_blank"><i class="fab fa-facebook-f"></i></a>
						<a href="https://twitter.com/EmpedradaLodge" target="_blank"><i class="fab fa-twitter"></i></a>
					</div>
				
				</div>
			</div>

			<div class="footer-copyrigth">
				<p>Copyright All Rigth Reserved By Empredada 2018</p>
			</div>

		</div>

	
		<div class="site-info">
			<div class="site-info-container container">
				<a href="<?php echo esc_url( __( 'http://www.sidequest.pe', 'empedrada' ) ); ?>" target="_blank">
					<span><?php echo pll__("Hecho por"); ?></span> <span style="font-weight: bold;">Sidequest</span>
<!-- 					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Hecho por Sidequest' ), '' );
					?> -->
				</a>
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					?>
			</div>
		</div><!-- .site-info -->

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
