<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Empedrada_Lodge
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header container">
		
		<div class="main-header">
		
		<!--INI DESKTOP VIEW MENU -->

			<div class="empedrada-header-view-desktop row">
				
				<!-- HEADER FLOAT BRAND -->
				<div class="site-branding col-lg-2 col-md-2">
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) :
						?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif;
					$empedrada_description = get_bloginfo( 'description', 'display' );
					if ( $empedrada_description || is_customize_preview() ) :
						?>
						<p class="site-description"><?php echo $empedrada_description; /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>
				</div><!-- .site-branding -->

				<div class="space-placeholder col-lg-2 col-md-2"></div>

				<div class="site-menu-container row col-lg-8 col-md-8">
					<nav id="site-navigation" class="main-navigation col-lg-8">
						<?php
						wp_nav_menu( array(
							'menu_class' => 'hidden-sm-down primary-menu-desktop',
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
						?>
					</nav><!-- # Desktop #site-navigation -->
					
					<div class="nav-social-icons col-lg-2">
						<ul>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>

					<div class="nav-language col-lg-2">
					<!-- LANG MENU -->
						<?php if ( is_active_sidebar( 'empedrada-side-bar' ) ) : ?>
						    <?php dynamic_sidebar( 'empedrada-side-bar' ); ?>
						<?php endif; ?>
					<!-- LANG MENU -->
					</div>
				</div>

				<div class="site-call-to-action col-lg-2 col-md-2">
					<a href="<?php empedrada_enlace_reserva(); ?>"><p><?php echo pll__("Reservar Online"); ?></p></a>
				</div>
			
			</div>

		<!-- ENDS DESKTOP VIEW MENU -->




		<!-- =======================
			 INI DESKTOP VIEW MENU
			 ======================= -->

			<div class="empedrada-header-view-mobile row">
				
				<!-- HEADER FLOAT BRAND -->
				<div class="site-branding-mobile col-4">
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) :
						?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					else :
						?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
						<?php
					endif;
					$empedrada_description = get_bloginfo( 'description', 'display' );
					if ( $empedrada_description || is_customize_preview() ) :
						?>
						<p class="site-description"><?php echo $empedrada_description; /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>
				</div><!-- .site-branding -->


				<div class="header-mobile-col col-8">
					
					<div class="site-menu-container row">
						<nav id="site-navigation" class="main-navigation col-lg-8">
							<button class="navbar-toggler navbar-mobile" type="button" data-target="#navbar-header" data-toggle="collapse" aria-controls="navbar-header">&#9776;</button>
							
							<div class="collapse navbar-toggleable-xs" id="navbar-header">
								<a class="navbar-brand" href="#"></a>
								<?php
								wp_nav_menu( array(
									'theme_location' => 'menu-1',
									'menu_id'        => 'mobile-menu',
								) );
								?>
							</div>
						</nav><!-- # mobile #site-navigation -->
					</div>

					<div class="nav-social-icons">
						<ul>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>

					<div class="nav-language">
					<!-- LANG MENU -->
						<?php if ( is_active_sidebar( 'empedrada-en-side-bar' ) ) : ?>
						    <?php dynamic_sidebar( 'empedrada-en-side-bar' ); ?>
						<?php endif; ?>
					<!-- LANG MENU -->
					</div>
				
				</div>



				<div class="site-call-to-action col-12">
					<a href="<?php empedrada_enlace_reserva(); ?>"><p><?php echo pll__("Reservar Online"); ?></p></a>
				</div>
			
			</div>


		<!-- =======================
			 ENDS DESKTOP VIEW MENU
			 ======================= -->

		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
