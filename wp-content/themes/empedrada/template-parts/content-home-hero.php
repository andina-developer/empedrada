<div class="empedrada-section empedrada-slider empedrada-hero">
	<div class="empedrada-hero-slider empedrada-homePage-banner">
		<img src="<?php echo the_field('home_header_banner'); ?>" alt="">
		<h1><?php echo the_field('texto_principal'); ?></h1>
	</div>
	<div class="empedrada-hero-reservation-container container">
		<div class="empedrada-hero-reservation-item">
			<h2><?php echo pll__("Haz tu reserva Online"); ?></h2>
			<a href="<?php empedrada_enlace_reserva(); ?>" class="button-brand"><p><?php echo pll__("Reservar"); ?></p></a>
		</div>
	</div>
</div>