<div class="empedrada-section empedrada-map">
	<div class="empedrada-map-container container">
		<div class="empedrada-map-main row">
			<div class="map-item map-address col-lg-6 col-sm-12">
				<div class="map-address-title">
					<h2><?php echo pll__('Hotel en Caral, Lima'); ?></h2>
				</div>
				<div class="map-address-desc">
					<p><?php echo pll__('rodeado de una impresionante vista al valle de caral y a los campos de cultivo de paltos'); ?></p>
				</div>
				<div class="map-address-address">
					<h3><?php echo pll__('Dirección'); ?><br/>Mz s/n  Lote A2 - Fundo La Empedrada </h3>
					<h3>Caral, Lima, Perú</h3>
					<h3><?php echo pll__('Teléfono'); ?><br/> 00511 2745974</h3>
					<h3><?php echo pll__('Coordenadas'); ?> <br/> Latitud: 10° 55.545 Sur</h3>
				</div>
			</div>
			<div class="map-item map-visual col-lg-6">

			</div>
		</div>
	</div>
	<div class="map-item-img col-sm-12">
		<img src="<?php echo the_field('ubicacion_mapa'); ?>" alt="">
	</div>	
</div>