<div class="empedrada-section empedrada-gallery">
	<div class="empedrada-gallery-container container">
		<div class="empedrada-gallery-title">
			<h2><?php echo pll__("Inspirate") ?></h2>
		</div>
		<div class="empedrada-gallery-main">
			
			<div class="gallery-col gallery-one-two">
				<div class="gallery-item g-item-1">
					<img src="<?php echo the_field('home_gallery_pic_50'); ?>" alt="">
				</div>
			</div>

			<div class="gallery-col gallery-one-four">
				<div class="gallery-item g-item-4">
					<img src="<?php echo the_field('home_gallery_pic_25-1'); ?>" alt="">
				</div>
				<div class="gallery-item g-item-4">
					<img src="<?php echo the_field('home_gallery_pic_25-2'); ?>" alt="">
				</div>
				<div class="gallery-item g-item-4">
					<img src="<?php echo the_field('home_gallery_pic_25-3'); ?>" alt="">
				</div>
				<div class="gallery-item g-item-4">
					<img src="<?php echo the_field('home_gallery_pic_25-4'); ?>" alt="">
				</div>

			</div>
			
		</div>
		<div class="empedrada-gallery-more">
			<a href="<?php echo get_site_url(); ?>/<?php empedrada_link_gallery (); ?>"><h3><?php echo pll__("Ver más") ?></h3></a>
		</div>
	</div>
</div>