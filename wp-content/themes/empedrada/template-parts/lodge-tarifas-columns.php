	
	<?php setup_postdata($post); ?>
	<div class="carousel-tarifas">
	
		<div class="tarifa-description carousel-tarifas-item container">
			<div class="row">
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<div class="col-lg-4">
					<div class="lodge-room-box">
						<h2><?php the_title(); ?></h2>
						<p><?php the_excerpt(); ?></p>
						<!-- <p class="room-price"><?php echo the_field('room_price'); ?></p> -->
					</div>
				</div>
			<?php endforeach; ?>	
				<div class="button-transparent button-bookig-lodge">
					<a href="<?php empedrada_enlace_reserva(); ?>">
						<p><?php echo pll__("Reservar") ?></p>
					</a>
				</div>
		</div>

	</div>