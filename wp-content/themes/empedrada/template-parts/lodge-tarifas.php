<div class="empedrada-section empedrada-lodge-tarifas">
	<div class="empedrada-lodge-tarifa-container container">
		
		<div class="tarifa-title">
			<h2><?php echo pll__("Tarifas del Hotel") ?></h2>
			<p><?php echo pll__("CONSULTA LAS TARIFAS DEL HOTEL") ?></p>
		</div>
		
		<div class="tarifa-habitacion-main">
			
			<?php $posts = get_field('habitaciones_promocion'); 

			if( $posts ): ?>

			<!-- POST  -->
				<div class="empredrada-offers-col offers-col-img">
					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner">
						
						<!-- START MODULE SLIDER -->
						
						

						<!-- START TREE COLUMNS -->

						<?php include 'lodge-tarifas-columns.php'; ?>
					  </div>
					</div>
				</div>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>	


		</div>
	</div>
</div>