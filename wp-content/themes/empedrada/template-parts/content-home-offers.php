<div class="empedrada-section empedrada-offers container">
	
	<div class="empedrada-offers-container row">
		<div class="empredrada-offers-col offers-col-txt col-lg-6">
			<div class="offer-text">
				<h2><?php echo pll__("Promociones") ?></h2>
				<h3><?php echo pll__("BENEFICIESE DE NUESTRAS OFERTAS Y PROMOCIONES Y DESCRUBRA CARAL AL MEJOR PREIO, ¡LE ESTAMOS ESPERANDO!") ?></h3>
			</div>
			<div class="offer-control-slider">
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>				
			</div>
		</div>

		<?php $posts = get_field('habitaciones_promocion'); 

		if( $posts ): ?>

			<div class="empredrada-offers-col offers-col-img col-lg-6">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
					<?php $i=0; ?>
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
		        		
		        		<?php setup_postdata($post); ?>
						
						<div class="carousel-item <?php echo ($i==0)?'active':''; ?>">
						  <a href="<?php the_permalink(); ?>"> 
						  	<?php the_post_thumbnail('525px', array('class' => 'd-block w-100') ); ?>
						  </a>
						</div>
					<?php $i++;  ?>
					<?php endforeach; ?>
				  </div>
				</div>
			</div>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>	
		
	</div>
</div>