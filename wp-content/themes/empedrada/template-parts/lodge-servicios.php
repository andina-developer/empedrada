<div class="empedrada-section empedrada-lodge-servicios">
	<div class="empedrada-lodge-servicios-container container">
		<div class="servicios-title">
			<h2><?php echo pll__("Servicios del Hotel") ?></h2>
			<h3><?php echo pll__("ESTOS SON NUESTROS SERVICIOS") ?></h3>
		</div>
		<div class="servicios-items-main row">
			
			<!-- SERVICIOS REPLACE LOOP -->

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_1'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Parking"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_2'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Admite pago con tarjetas"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_3'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Piscina"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_4'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Servicio de Limpieza"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_5'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Sabanas y Toallas"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_6'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Ventilador en las habitaciones"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_7'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Consigna de equipajes"); ?></p>
				</div>
			</div>

			<div class="servicios-item-ico col-lg-3 col-6">
				<div class="ico-img">
					<img src="<?php echo the_field('icono_servicio_8'); ?>" alt="">
				</div>
				<div class="ico-desc">
					<p><?php echo pll__("Jardín"); ?></p>
				</div>
			</div>			

		<!-- SERVICIOS REPLACE LOOP -->

		</div>
	</div>	
</div>