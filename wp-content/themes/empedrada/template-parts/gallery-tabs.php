<div class="gallery-container container">
	<ul class="nav nav-pills mb-3 gallery-bar-category" id="pills-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="pills-Hotel-tab" data-toggle="pill" href="#pills-Hotel" role="tab" aria-controls="pills-Hotel" aria-selected="true">Hotel</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pills-habitaciones-tab" data-toggle="pill" href="#pills-habitaciones" role="tab" aria-controls="pills-habitaciones" aria-selected="false"><?php echo pll__("Habitaciones") ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pills-piscina-tab" data-toggle="pill" href="#pills-piscina" role="tab" aria-controls="pills-piscina" aria-selected="false"><?php echo pll__("Piscina") ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pills-baths-tab" data-toggle="pill" href="#pills-baths" role="tab" aria-controls="pills-baths" aria-selected="false"><?php echo pll__("Baños") ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="pills-vista-tab" data-toggle="pill" href="#pills-vista" role="tab" aria-controls="pills-vista" aria-selected="false"><?php echo pll__("Vista") ?></a>
		</li>			
	</ul>
	<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-Hotel" role="tabpanel" aria-labelledby="pills-hotel-tab">
			<?php include 'gallery-hotel.php'; ?>
		</div>
		<div class="tab-pane fade" id="pills-habitaciones" role="tabpanel" aria-labelledby="pills-habitaciones-tab">
			<?php include 'gallery-habitaciones.php'; ?>
		</div>
		<div class="tab-pane fade" id="pills-piscina" role="tabpanel" aria-labelledby="pills-piscina-tab">
			<?php include 'gallery-piscina.php'; ?>
		</div>
		<div class="tab-pane fade" id="pills-baths" role="tabpanel" aria-labelledby="pills-baths-tab">
			<?php include 'gallery-baths.php'; ?>
		</div>
		<div class="tab-pane fade" id="pills-vista" role="tabpanel" aria-labelledby="pills-vista-tab">
			<?php include 'gallery-vista.php'; ?>
		</div>	
	</div>
</div>