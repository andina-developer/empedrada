<div class="empredara-section empedrada-features">
	<div class="empedrada-features-container container">
		<div class="empedrada-features-title">
			<h2 class="title-caral">Caral</h2>
			<p>Lima - Perú</p>
		</div>

		<div class="empedrada-feature-item row">
			<div class="col-lg-4 col-md-4">
				<div class="feature-img">	
					<img src="<?php echo empedrada_translate('home_features_icon_1', 'home_features_icon_1-en'); ?>" alt="">
				</div>
				<div class="feature-text">
					<p><?php empedrada_translate('home_features_text_1', 'home_features_text_1-en') ?></p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="feature-img">	
					<img src="<?php echo empedrada_translate('home_features_icon_2', 'home_features_icon_2-en'); ?>" alt="">
				</div>
				<div class="feature-text">
					<p><?php empedrada_translate('home_features_text_2', 'home_features_text_2-en') ?></p>
				</div>			
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="feature-img">	
					<img src="<?php echo empedrada_translate('home_features_icon_3', 'home_features_icon_3-en'); ?>" alt="">
				</div>
				<div class="feature-text">
					<p><?php empedrada_translate('home_features_text_3', 'home_features_text_3-en') ?></p>
				</div>			
			</div>
		</div>
	</div>
</div>