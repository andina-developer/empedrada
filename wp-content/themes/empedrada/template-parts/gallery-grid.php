
<?php 

$posts = get_field('galeria_hotel');
if( $posts ): ?>
	<div class="empedrada-section container">
		<div class="empedrada-section-row row">
		<!-- CONTENIDO -->
		<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
			<div class="gallery-page-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<?php the_post_thumbnail('250px'); ?>
			</div>
		<!-- CONTENIDO -->
		<?php endforeach; ?>
		</div>
	</div>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>		


