<div class="empedrada-section empedrada-cta-background empedrada-cta">
	<div class="empedrada-cta-container container">
		<div class="empedrada-cta-title">
			<h2><?php echo pll__("¿Qué esperas? has tu reservación ahora"); ?></h2>
		</div>
		<div class="empedrada-cta-button">
			<a href="<?php empedrada_enlace_reserva(); ?>" class="button-brand">
				<p><?php echo pll__("Reservar"); ?></p>
			</a>
		</div>
	</div>
</div>