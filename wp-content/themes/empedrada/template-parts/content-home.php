<?php 

// HOME HERO
	include 'content-home-hero.php';

// HOME FEATURES
	include 'content-home-features.php';

// HOME GALLERY
	include 'content-home-gallery.php';

// HOME OFFERS
	include 'content-home-offers.php';

// HOME VIDEO
	include 'content-home-video.php';

// HOME MAP
	include 'content-home-map.php';	

// MODULE CTA
	include 'content-module-cta.php';
