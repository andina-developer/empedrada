<div class="empedrada-section empedrada-lodge-location">
	<div class="empedrada-lodge-location-container container">
		<div class="location-title">
			<h2><?php echo pll__("Locación") ?></h2>
			<h3><?php echo pll__("RODEADO DE UNA IMPRESIONANTE VISTA AL VALLE DE CARAL Y A LOS CAMPOS DE CULTIVO DE PALTOS") ?></h3>
		</div>
		<div class="location-main row">
			<div class="location-address col-lg-12 row">
				<div class="col-lg-4">
					<h3><?php echo pll__("Dirección") ?></h3>
					<h3>Mz s/n Lote A2 - Fundo la Empedrada</h3>
					<h3>Caral, Lima - Perú</h3>
				</div>
				<div class="col-lg-4">
					<h3><?php echo pll__("Teléfono") ?></h3>
					<h3>0051 1 2745974</h3>
				</div>
				<div class="col-lg-4">
					<h3><?php echo pll__("Coordenadas") ?></h3>
					<h3>Latitud 10° 55.5545 Sur</h3>
				</div>
			</div>
			<div class="col-lg-12 location-view">
				<img src="<?php echo the_field('lodge_locacion'); ?>" alt="">
			</div>
		</div>
	</div>
</div>	