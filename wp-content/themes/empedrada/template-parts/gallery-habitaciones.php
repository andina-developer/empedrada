
<?php 

$posts = get_field('galeria_habitaciones');
if( $posts ): ?>
	<div class="empedrada-section container">
		<div class="empedrada-section-row row">
		<!-- CONTENIDO -->
		<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
			<div class="gallery-page-item col-lg-4 col-md-4 col-sm-4 col-xs-6">
				<a href="" data-toggle="modal" data-target="#exampleModal-<?php the_id();?>">
					<?php the_post_thumbnail('250px'); ?>
				</a>
			</div>
			<!-- CONTENIDO -->
			<!-- Modal -->
			<div class="modal fade" id="exampleModal-<?php the_id();?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-body">
			        <?php the_post_thumbnail('250px'); ?>
			      </div>
			    </div>
			  </div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
	<!-- MODAL BOX -->



    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>		


