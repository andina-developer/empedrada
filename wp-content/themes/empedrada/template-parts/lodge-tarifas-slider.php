<?php $i=0; ?>
<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	
	<?php setup_postdata($post); ?>
	<div class="carousel-item carousel-tarifas <?php echo ($i==0)?'active':''; ?>">
	
		<div class="tarifa-description carousel-tarifas-item row">
			<div class="col-lg-6">
				<h2><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
				<p class="room-price"><?php echo the_field('room_price'); ?></p>
			<div class="button-transparent"><a href="#"><p><?php echo pll__("Reservar") ?></p></a></div>
			</div>
			<div class="col-lg-6">
				<a href="<?php the_permalink(); ?>"> 
					<?php the_post_thumbnail('525px', array('class' => 'd-block w-100') ); ?>
				</a>
			</div>
		</div>

	</div>
<?php $i++;  ?>
<?php endforeach; ?>