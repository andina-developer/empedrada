<?php
/**
 * Empedrada Lodge functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Empedrada_Lodge
 */

if ( ! function_exists( 'empedrada_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function empedrada_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Empedrada Lodge, use a find and replace
		 * to change 'empedrada' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'empedrada', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'empedrada' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'empedrada_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'empedrada_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function empedrada_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'empedrada_content_width', 640 );
}
add_action( 'after_setup_theme', 'empedrada_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

require get_template_directory() . '/inc/sidebars.php';

/**
 * Enqueue scripts and styles.
 */
function empedrada_scripts() {
	wp_enqueue_style( 'empedrada-bootstrap-css', get_template_directory_uri(). '/lib/bootstrap-4.0.0/css/bootstrap.css' );

	wp_enqueue_style( 'empedrada-style', get_stylesheet_uri() );

	wp_enqueue_script( 'empedrada-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'empedrada-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'empedrada-jquery', get_template_directory_uri() . '/lib/jquery-3.3.1/jquery-3.3.1.min.js', array( 'jquery' ), '20151215', true  );

	// FONTS AWESOME
	wp_enqueue_script( 'empedrada-fontawesome', get_template_directory_uri() . '/lib/fontawesome-free-5.0.9/js/fontawesome-all.min.js' );
	
	wp_enqueue_script( 'empedrada-popper-js', get_template_directory_uri() . '/lib/bootstrap-4.0.0/popper-js/popper.js', array( 'jquery' ), '20151215', true  );

	wp_enqueue_script( 'empedrada-bootstrap-js', get_template_directory_uri() . '/lib/bootstrap-4.0.0/js/bootstrap.min.js', array( 'jquery' ), '20151215', true  );

	wp_enqueue_script( 'empedrada-app-js', get_template_directory_uri() . '/js/app.js', array( 'jquery' ), '20151215', true  );	
}
add_action( 'wp_enqueue_scripts', 'empedrada_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

require get_template_directory() . '/inc/string-lang.php';


//SUPPORT FOR POLLYLANG

if( function_exists('acf_add_options_page') ) {
  // Language Specific Options
  // Translatable options specific languages. e.g., social profiles links
  // 
  
  $languages = array( 'en', 'es' );
  foreach ( $languages as $lang ) {
    acf_add_options_page( array(
      'page_title' => 'Site Options (' . strtoupper( $lang ) . ')',
      'menu_title' => __('Site Options (' . strtoupper( $lang ) . ')', 'text-domain'),
      'menu_slug'  => "site-options-${lang}",
      'post_id'    => $lang
    ) );
  }
}


// HOW TO USE
//
// empedrada_translate($es = string,$en = string,);

function empedrada_translate ($es, $en) {

	if (pll_current_language() == 'es') { 
		// echo the_field('home_features_text_1'); 
		echo the_field($es);
	} elseif (pll_current_language() == 'en') {
		// echo the_field('home_features_text_1-en'); 
		echo the_field($en);
	} else{
		// echo the_field('home_features_text_1'); } 		
		echo the_field($es);
	}
	return;
}	

function empedrada_link_gallery () {

	if (pll_current_language() == 'es') { 
		// echo the_field('home_features_text_1'); 
		echo 'es/galeria';
	} elseif (pll_current_language() == 'en') {
		// echo the_field('home_features_text_1-en'); 
		echo 'en/gallery';
	} else{
		// echo the_field('home_features_text_1'); } 		
		echo 'es/galeria';
	}
	return;
}	

// add_action( 'after_setup_theme', 'empedrada_translate' );

// CUSTOM SIDEBAR HEADER

function empedrada_header_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Header', 'empedrada' ),
            'id' => 'empedrada-side-bar',
            'description' => __( 'Header SideBar', 'empedrada' ),
            'before_widget' => '<div class="select-lang-emp widget-content">',
            'after_widget' => "</div>",
            'before_title' => '',
            'after_title' => '',
        )
    );
}
add_action( 'widgets_init', 'empedrada_header_sidebar' );


function empedrada_header_en_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Cambiar Idioma Eng', 'empedrada-en' ),
            'id' => 'empedrada-en-side-bar',
            'description' => __( 'Cambiar Idioma Engr', 'empedrada-en' ),
            'before_widget' => '<div class="select-lang-emp widget-content">',
            'after_widget' => "</div>",
            'before_title' => '',
            'after_title' => '',
        )
    );
}
add_action( 'widgets_init', 'empedrada_header_en_sidebar' );





// LINK BOOKING

function empedrada_booking_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Booking Link', 'empedrada-booking-link' ),
            'id' => 'empedrada-booking-link',
            'description' => __( 'Booking Link', 'empedrada-booking-link' ),
            'before_widget' => '<div class="select-lang-emp widget-content">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => '',
        )
    );
}
add_action( 'widgets_init', 'empedrada_booking_sidebar' );



//REMOVE AUTOTAG

remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

// ADD CUSTOM LOGO ADMIN

function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/assets/images/demo/logo-admin.png) !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}


//


add_action( 'customize_register', 'mitema_customize_register' );

function mitema_customize_register($wp_customize) {

$wp_customize->add_section( 'booking_link', array(
    'title'          => __( 'Enlace de Reserva', 'enlacedereserva' ),
    'priority'       => 35
) );

$wp_customize->add_setting('enlace_de_reserva[enlace_reserva]', array(
    'default'        => 'https://hotels.cloudbeds.com/reservation/WDxVjz#checkin=2018-03-21&checkout=2018-03-24',
    'capability'     => 'edit_theme_options',
    'type'           => 'option',
));

$wp_customize->add_control('enlace_reserva', array(
    'label'      => __('Enlace de Reserva', 'enlacedereserva'),
    'section'    => 'booking_link',
    'settings'   => 'enlace_de_reserva[enlace_reserva]',
));

}

function empedrada_enlace_reserva() {
    $datos = get_option('enlace_de_reserva');
    $name_texto = $datos['enlace_reserva'];
 	
	echo $datos[enlace_reserva];
}
// add_action( 'wp_head', 'empedrada_enlace_reserva' ); 