<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Empedrada_Lodge
 */

get_header();
?>

<div class="empedrada-section empedrada-room-header">
	<div class="empedrada-room-header-container container">
		<h2><?php echo pll__("Tarifas del Hotel"); ?></h2>
		<h3><?php echo pll__("CONSULTA LAS TARFIAS DEL HOTEL"); ?></h3>
	</div>
	<div class="empedrada-room-desc container">
		
		<div class="empedrada-room-desc row">
			<div class="room-desc col-lg-6">
				<h2><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
	            <div class="button-transparent">
	            	<a href="<?php echo the_field('room_link');?>"><?php echo pll__("ver disponibilidad"); ?></a>
	            </div>

			</div>
			<div class="room-desc col-lg-6">
				<div class="room-thumbnail">
					<?php the_post_thumbnail(''); ?>
				</div>
			</div>
		</div>
	
		


	</div>
	</div>
</div>

	<div id="primary" class="content-area container single-habitacion">
		<main id="main" class="site-main">

		<?php
		// while ( have_posts() ) :
		// 	the_post();

		// 	get_template_part( 'template-parts/content', get_post_type() );

			

			// // If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		//endwhile; // End of the loop.
		?>
		


		</main><!-- #main -->
	</div><!-- #primary -->

<div class="empedrada-section empredada-aditionals">
	<div class="features-container container">
		<div class="empedrada-room-acomodaciones">
			<div class="acomodaciones-title">
				<h2><?php echo pll__("ACOMODACIONES"); ?></h2>
				<h3><?php echo pll__("DE HABITACIÓN"); ?></h3>
			</div>
			
			<div class="acomodaciones-ico-container row">

				<div class="col-lg-1 hidden-sm"></div>
				<div class="acomodaciones-content col-lg-2 col-6">
					<div class="ico-img">
						<!-- <img src="<?php echo the_field('room_ico_1'); ?>" alt=""> -->
						
						<img src="<?php echo empedrada_translate('room_ico_1', 'room_ico_1-en'); ?>" alt="">
					</div>
					<div class="ico-desc">
						<p><?php echo empedrada_translate('room_txt_1', 'room_txt_1-en'); ?></p>
					</div>
				</div>

				<div class="acomodaciones-content col-lg-2 col-6">
					<div class="ico-img">
						<img src="<?php echo empedrada_translate('room_ico_2', 'room_ico_2-en'); ?>" alt="">
					</div>
					<div class="ico-desc">
						<p><?php echo empedrada_translate('room_txt_2', 'room_txt_2-en'); ?></p>
					</div>
				</div>

				<div class="acomodaciones-content col-lg-2 col-6">
					<div class="ico-img">
						<img src="<?php echo empedrada_translate('room_ico_3', 'room_ico_3-en'); ?>" alt="">
					</div>
					<div class="ico-desc">
						<p><?php echo empedrada_translate('room_txt_3', 'room_txt_3-en'); ?></p>
					</div>
				</div>
				<div class="acomodaciones-content col-lg-2 col-6">
					<div class="ico-img">
						<img src="<?php echo empedrada_translate('room_ico_4', 'room_ico_4-en'); ?>" alt="">
					</div>
					<div class="ico-desc">
						<p><?php echo empedrada_translate('room_txt_4', 'room_txt_4-en'); ?></p>
					</div>
				</div>

				<div class="acomodaciones-content col-lg-2 col-6">
					<div class="ico-img">
						<img src="<?php echo empedrada_translate('room_ico_5', 'room_ico_5-en'); ?>" alt="">
					</div>
					<div class="ico-desc">
						<p><?php echo empedrada_translate('room_txt_5', 'room_txt_5-en'); ?></p>
					</div>
				</div>	
			</div>											

		</div>

		<div class="acomodaciones-room-separator">
			
		</div>

		<div class="empedrada-room-info-aditional row">
			<?php echo empedrada_translate('room_add_info', 'room_add_info-en'); ?>
		</div>
	</div>
</div>


<?php 

$posts = get_field('habitaciones_recomendadas');

if( $posts ): ?>
	<div class="empedrada-section empedrada-room">
		<div class="empedrada-room-title">
			<h2><?php echo pll__("Otras recomendaciones") ?></h2>
		</div>
	    <div class="room-recomendend-container container">
	    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	        <?php setup_postdata($post); ?>
	        <div class="room-recomendend-item">
	            <a href="<?php the_permalink(); ?>">
	            	<!-- <?php the_title(); ?> -->
	            	<?php the_post_thumbnail('250px'); ?>
	            </a>
	            <p><?php the_excerpt(); ?></p>
	            <div class="room-price">
	            	<p>S/. <?php echo the_field('room_price'); ?></p>
	            </div>
	            <div class="button-brand">
	            	<a href="<?php echo the_field('room_link');?>"><?php echo pll__("Reservar") ?></a>
	            </div>

	        </div>

	    <?php endforeach; ?>
	    </div>
	</div>	    
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>		




<?php
get_sidebar();
get_footer();
