<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * 
 * Template Name: Empedrada Pruebas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Empedrada_Lodge
 */

get_header();
?>
	<div class="empedrada-page-banner-header">
		<img src="<?php echo the_field('page-image-header');?>" alt="">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		endwhile; // End of the loop.
		?>

<?php 
$args = array(
  'public'   => true,
  '_builtin' => false
   
); 
$output = 'names'; // or objects
$operator = 'and'; // 'and' or 'or'
$taxonomies = get_taxonomies( $args, $output, $operator ); 
if ( $taxonomies ) {
    echo '<ul>';
    foreach ( $taxonomies  as $taxonomy ) {
        echo '<li>' . $taxonomy . '</li>';
    }
    echo $taxonomy;
    echo '</ul>'; 

}

$args = array(
    'name' => 'category_gallery'
);



$output = 'objects'; // or names
$taxonomies= get_taxonomies( $args, $output ); 
if ( $taxonomies ) {
    foreach ( $taxonomies as $taxonomy ) {
        echo '<div>' . $taxonomy->labels->name . '</div>';
        echo $taxonomies;
    }
}  

?>





		</main><!-- #main -->
	</div><!-- #primary -->
		


<?php
get_sidebar();
get_footer();
