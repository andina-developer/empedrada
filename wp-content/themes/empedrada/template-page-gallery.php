<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 * 
 * Template Name: Empedrada Gallery
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Empedrada_Lodge
 */

get_header();
?>
	<div class="empedrada-page-banner-header">
		<img src="<?php echo the_field('page-image-header');?>" alt="">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main">

		<?php 
		get_template_part( 'template-parts/gallery', 'tabs' );
		 ?>

		<?php
		while ( have_posts() ) :
			the_post();


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
		
	<?php
	
	?>

<?php
get_sidebar();
get_footer();
